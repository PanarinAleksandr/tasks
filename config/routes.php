<?php

use App\Controllers\AuthController;
use App\Controllers\HomeController;
use App\Controllers\TaskController;
use FastRoute\RouteCollector;

return FastRoute\simpleDispatcher(function (RouteCollector $r) {

    $r->get('/{pageId:[0-9]+}', [
        'class' => HomeController::class,
        'method' => 'index'
    ]);
    $r->get('/auth', [
        'class' => AuthController::class,
        'method' => 'index'
    ]);
    $r->get('/create-admin', [
        'class' => AuthController::class,
        'method' => 'createAdmin'
    ]);
    $r->post('/add-task', [
        'class' => TaskController::class,
        'method' => 'addTask'
    ]);
    $r->addRoute(['GET', 'POST'], '/edit-task/{taskId: [0-9]+}', [
        'class' => TaskController::class,
        'method' => 'editTask'
    ]);
    $r->post('/login', [
        'class' => AuthController::class,
        'method' => 'login'
    ]);
    $r->get('/logout', [
        'class' => AuthController::class,
        'method' => 'logout'
    ]);
});