<?php

namespace App\Kernel;

use FastRoute\Dispatcher;

class Kernel
{
    private $routes;
    private $uri;
    private $httpMethod;

    public function __construct()
    {
        $this->routes = include BASE_DIR . '/config/routes.php';
        $this->httpMethod = $_SERVER['REQUEST_METHOD'];

        $uri = $_SERVER['REQUEST_URI'];
        if (false !== $pos = strpos($uri, '?')) {
            $uri = substr($uri, 0, $pos);
        }
        $this->uri = rawurldecode($uri);
    }

    public function run(): void
    {
        $uri = str_replace('/', '', $this->uri);

        if ($uri == '') {
            header("Location: /1");
            die;
        }

        $routeInfo = $this->routes->dispatch($this->httpMethod, $this->uri);

        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                // ... 404 Not Found
                header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found", true, 404);
                break;
            case Dispatcher::METHOD_NOT_ALLOWED:
                $allowedMethods = $routeInfo[1];
                // ... 405 Method Not Allowed
                header($_SERVER["SERVER_PROTOCOL"] . " 405 Method Not Allowed", true, 405);
                break;
            case Dispatcher::FOUND:
                $handler = $routeInfo[1];
                $vars = $routeInfo[2];
                $class = $handler['class'];
                $method = $handler['method'] . 'Action';
                $request = new Request($vars);
                $controller = new $class();

                echo call_user_func_array([$controller, $method], [$request]);
                break;
            default:
                header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found", true, 404);
                break;
        }
    }
}