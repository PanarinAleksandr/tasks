<?php


namespace App\Kernel;


class Request
{
    private $request = [];

    public function __construct(array $data = null)
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $url_components = parse_url($_SERVER['REQUEST_URI']);
            if (isset($url_components['query'])) {
                parse_str($url_components['query'], $params);
                $this->request = $params;
            }
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (json_decode(file_get_contents("php://input"), true)) {
                $this->request = json_decode(file_get_contents("php://input"), true);
            } else {
                $this->request = $_POST;
            }
        }

        if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_FILES) {
            $this->request = array_merge($this->request, $_FILES);
        }

        if ($data) {
            $this->request = array_merge($this->request, $data);
        }
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->request;
    }
}