<?php

namespace App\Kernel;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager as Manager;

class EntityManager
{
    /**
     * @return Manager
     * @throws \Doctrine\ORM\ORMException
     */
    public static function init()
    {
        $isDevMode = false;
        $proxyDir = __DIR__ . "/../Proxies";
        $cache = new \Doctrine\Common\Cache\ArrayCache;
        $useSimpleAnnotationReader = false;

        $config = Setup::createAnnotationMetadataConfiguration(array(__DIR__ . "/../Entities"),
            $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);
        $config->setAutoGenerateProxyClasses(true);

        $connection = [
            'dbname' => 'test_db',
            'user' => 'root',
            'password' => 'secret123',
            'host' => 'mysql',
            'driver' => 'pdo_mysql'
        ];
        return Manager::create($connection, $config);
    }
}