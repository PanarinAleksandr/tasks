<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="tasks")
 * @ORM\Entity(repositoryClass="App\Repositories\TaskRepository")
 **/
class Task
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $text
     * @ORM\Column(type="text", name="text")
     */
    private $text;

    /**
     * @var boolean $status
     * @ORM\Column(type="boolean", name="is_done")
     */
    private $isDone;
    /**
     * @var boolean $status
     * @ORM\Column(type="boolean", name="is_changed_by_admin")
     */
    private $isChangedByAdmin;

    /**
     * @var User|null
     * @ORM\ManyToOne(targetEntity="App\Entities\User", inversedBy="tasks")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function __construct()
    {
        $this->isDone = false;
        $this->isChangedByAdmin = false;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param $text
     * @return $this
     */
    public function setText($text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDone(): bool
    {
        return $this->isDone;
    }

    /**
     * @param bool $isDone
     * @return $this
     */
    public function setIsDone(bool $isDone): self
    {
        $this->isDone = $isDone;

        return $this;
    }

    /**
     * @return bool
     */
    public function isChangedByAdmin(): bool
    {
        return $this->isChangedByAdmin;
    }

    /**
     * @param bool $isChangedByAdmin
     * @return $this
     */
    public function setIsChangedByAdmin(bool $isChangedByAdmin): self
    {
        $this->isChangedByAdmin = $isChangedByAdmin;
        return $this;
    }


}