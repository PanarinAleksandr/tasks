<?php

namespace App\Controllers;

use App\Entities\Task;
use App\Kernel\Request;
use Doctrine\ORM\Tools\Pagination\Paginator;

class HomeController extends BaseController
{
    public function indexAction(Request $request)
    {
        $request = $request->getData();
        $page = intval($request['pageId']);
        $order = [];
        $orderName = strval($request['name']);
        $orderEmail = strval($request['email']);
        $orderStatus = strval($request['status']);

        $query = $this->em->getRepository(Task::class)->createQueryBuilder('t')
            ->leftJoin('t.user', 'u');
        if ($orderName) {
            $query->orderBy('u.name', $orderName);
            $order['name'] = $orderName;
        }
        if ($orderEmail) {
            $query->orderBy('u.email', $orderEmail);
            $order['email'] = $orderEmail;
        }
        if ($orderStatus) {
            $query->orderBy('t.isDone', $orderStatus);
            $order['status'] = $orderStatus;
        }
        $query->getQuery();
        $paginator = new Paginator($query);
        $paginator
            ->getQuery()
            ->setFirstResult(self::PAGE_SIZE_FOR_PAGINATOR * ($page - 1))
            ->setMaxResults(self::PAGE_SIZE_FOR_PAGINATOR);

        return $this->render('home/index.html.twig', [
            'tasks' => $paginator,
            'pagination' => $this->getPaginationHtml($paginator, $page, $order),
            'order' => $order,
            'currentPage' => $page
        ]);
    }
}