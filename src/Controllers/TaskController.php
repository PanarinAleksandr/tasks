<?php

namespace App\Controllers;

use App\Entities\Task;
use App\Entities\User;
use App\Kernel\Request;

class TaskController extends BaseController
{
    public function addTaskAction(Request $request)
    {
        $data = $request->getData();
        $name = trim(strval($data['name']));
        $email = trim(strval($data['email']));
        $text = trim(strval($data['text']));

        if (empty($name) || empty($email) || empty($text)) {
            return $this->response(['message' => 'Все поля должны быть заполнены'], 400);
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->response(['message' => 'Введите корректную почту'], 400);
        }
        $user = $this->em->getRepository(User::class)->findOneBy([
            'email' => $email
        ]);
        try {
            if (!$user) {
                $user = (new User())->setEmail($email)->setName($name);
                $this->em->persist($user);
            }
            $task = (new Task())->setText($text)->setUser($user);
            $this->em->persist($task);
            $this->em->flush();
        } catch (\Exception $e) {
            return $this->response(['message' => 'Произошла ошибка сохранения задачи'], 500);
        }

        return $this->response(['message' => 'Задача сохранена']);
    }

    public function editTaskAction(Request $request)
    {
        if ($this->checkAdmin()) {
            $successMessage = null;
            $request = $request->getData();
            $taskId = intval($request['taskId']);
            $task = $this->em->getRepository(Task::class)->find($taskId);

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $isDone = isset($request['isDone']);
                if ($task->getText() !== $request['text']) {
                    $task->setIsChangedByAdmin(true);
                }
                $task->setText($request['text'])->setIsDone($isDone);
                $this->em->persist($task);
                $this->em->flush();
                $successMessage = 'Данные успешно обновились';
            }

            return $this->render('task/edit.html.twig', [
                'task' => $task,
                'successMessage' => $successMessage
            ]);
        }
        header($_SERVER["SERVER_PROTOCOL"] . " 401", true, 401);
        die;
    }
}