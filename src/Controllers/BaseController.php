<?php

namespace App\Controllers;

use App\Entities\Admin;
use App\Entities\Task;
use App\Kernel\EntityManager;
use Doctrine\ORM\EntityManager as Manager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

abstract class BaseController
{
    const PAGE_SIZE_FOR_PAGINATOR = 3;
    /**
     * @var Manager
     */
    protected $em;
    /**
     * @var int
     */
    protected $userId;

    /**
     * BaseController constructor.
     * @throws ORMException
     */
    public function __construct()
    {
        $this->em = EntityManager::init();
        $this->userId = intval($_SESSION['userId']);
    }

    /**
     * @param string $template
     * @param array $variables
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function render(string $template, array $variables = [])
    {
        $loader = new FilesystemLoader(PATH_TO_TEMPLATES);
        $twig = new Environment($loader, [
            'debug' => true,
        ]);
        $twig->addExtension(new DebugExtension());
        $twig->addGlobal('public', BASE_PATH . '/public');
        $twig->addGlobal('userId', $this->userId);
        $twig->addGlobal('basePath', BASE_PATH);

        return $twig->render($template, $variables);
    }

    /**
     * @param array $data
     * @param int $code
     * @return false|string
     */
    protected function response($data, int $code = 200)
    {
        header('Content-Type: application/json');
        http_response_code($code);

        return json_encode($data);
    }

    /**
     * @param string $location
     */
    protected function redirectTo(string $location)
    {
        header('Location: ' . $location);
        die;
    }

    /**
     * @return bool
     */
    protected function checkAdmin(): bool
    {
        if ($this->userId) {
            $admin = $this->em->getRepository(Admin::class)->find($this->userId);
            if ($admin) {
                return true;
            }
        }
        return false;
    }

    protected function getPaginationHtml(Paginator $paginator, int $currentPage, array $order)
    {
        $orderQuery = null;
        if ($order) {
            $orderQuery = '?' . http_build_query($order);
        }
        $totalItems = count($paginator);
        $pagesCount = ceil($totalItems / self::PAGE_SIZE_FOR_PAGINATOR);
        $nextPage = ($currentPage + 1) > $pagesCount ? 0 : $currentPage + 1;
        $previousPage = $currentPage - 1;

        return $this->render('parts/paginator.html.twig', [
            'currentPage' => $currentPage,
            'pagesCount' => $pagesCount,
            'previousPage' => $previousPage,
            'nextPage' => $nextPage,
            'orderQuery' => $orderQuery
        ]);
    }
}