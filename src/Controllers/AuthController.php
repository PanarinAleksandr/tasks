<?php

namespace App\Controllers;

use App\Entities\Admin;
use App\Kernel\Request;

class AuthController extends BaseController
{
    public function indexAction()
    {
        return $this->render('auth/index.html.twig');
    }

    public function loginAction(Request $request)
    {
        $request = $request->getData();
        $email = trim(strval($request['email']));
        $password = trim(strval($request['password']));

        if ($email && $password) {
            $admin = $this->em->getRepository(Admin::class)->findOneBy([
                'email' => $email,
                'password' => $password
            ]);
            if ($admin) {
                $_SESSION['userId'] = $admin->getId();
                return $this->response(['message' => 'Авторизация прошла успешно', 'redirectTo' => BASE_PATH]);
            }
            return $this->response(['message' => 'Неправильные реквизиты доступа'], 400);
        }
        return $this->response(['message' => 'Заполните все поля'], 400);
    }

    public function logoutAction()
    {
        session_destroy();
        $this->redirectTo(BASE_PATH);
        die;
    }
}