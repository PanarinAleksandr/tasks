$(document).ready(function () {
    const message = $('#message');

    $("form").submit(function (e) {
        e.preventDefault();
        const form = $(this);
        const path = form.data('path');

        $.ajax({
            type: "POST",
            url: path,
            cache: false,
            data: new FormData(this),
            processData: false,
            contentType: false,
            beforeSend: function (xhr, opts) {
                message.empty();
                message.hide();
                message.removeClass('alert-info');
                message.removeClass('alert-danger');
            },
            success: function (response) {
                if (response.message) {
                    message.show();
                    message.addClass('alert-info');
                    message.append(response.message);

                    setTimeout(function () {
                        if (response.redirectTo) {
                            location.href = response.redirectTo;
                        } else {
                            location.reload();
                        }
                    }, 2000);
                }
            },
            error: function (jqXHR, status) {
                message.show();
                message.addClass('alert-danger');
                try {
                    const data = jQuery.parseJSON(jqXHR.responseText);
                    if (jqXHR.status !== 400) {
                        message.append('Системная ошибка')
                    }
                    if (jqXHR.status === 400 && data.message) {
                        message.append(data.message)
                    }
                } catch (e) {
                    message.append('Системная ошибка')
                }
            }
        });
    });
});