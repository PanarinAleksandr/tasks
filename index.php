<?php

use App\Kernel\Kernel;

define('BASE_DIR', __DIR__);
define('PATH_TO_TEMPLATES', BASE_DIR . '/templates');
define("BASE_PATH",  'http://' . $_SERVER['HTTP_HOST']);

require __DIR__ . '/vendor/autoload.php';
ini_set('display_errors', 0);
session_start();
$kernel = new Kernel();
$kernel->run();